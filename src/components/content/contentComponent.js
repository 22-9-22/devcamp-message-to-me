import { Component } from "react";
import FormComponent from "../Form/FormComponent";
import ResultComponent from "../Result/ResultComponent";

class Content extends Component {
    constructor(props) {
        super(props);

        this.state = {
            inputMessage: "",
            outputMessage: [],
            likeDisplay : false
        }
    }

    inputMessageChangeHandler = (value) => {
        this.setState({
            inputMessage: value
        })
    }

    outputMessageChangeHandler = () => {
        if(this.state.inputMessage) {
            this.setState({
                outputMessage:[...this.state.inputMessage, this.state.inputMessage],
                likeDisplay : true
            })
        }
    }
    render() {
        return (
            <div>
                <FormComponent inputMessageProps={this.state.inputMessage} inputMessageChangeHandlerProps={this.inputMessageChangeHandler} outputMessageChangeHandlerProps={this.outputMessageChangeHandler}/>
                <ResultComponent outputMessageProps={this.state.outputMessage} likeDisplayProps = {this.state.likeDisplay}/>
            </div>
        )
    }
}

export default Content